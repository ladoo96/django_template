# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from jobApp.models import Hydjobs, Punejobs, Chennaijobs, Banglorejobs
# Register your models here.

class hydjobsAdmin(admin.ModelAdmin):
    list_display = ['date','company','title']
class punejobsAdmin(admin.ModelAdmin):
    list_display = ['date','company','title']
class chennaijobsAdmin(admin.ModelAdmin):
    list_display = ['date','company','title']
class banglorejobsAdmin(admin.ModelAdmin):
    list_display = ['date','company','title']

admin.site.register(Hydjobs,hydjobsAdmin )
admin.site.register(Punejobs,punejobsAdmin )
admin.site.register(Chennaijobs,chennaijobsAdmin )
admin.site.register(Banglorejobs, banglorejobsAdmin )
