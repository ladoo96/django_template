# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from jobApp.models import Hydjobs
from jobApp.models import Punejobs
from jobApp.models import Chennaijobs
from jobApp.models import Banglorejobs


# Create your views here.

def index(request):
    return render(request, 'jobApp/index.html')

def hydjobs(request):
    hyd_list = Hydjobs.objects.all()
    my_dict = {'hyd_list': hyd_list}
    
    return render(request, 'jobApp/hydjobs.html', context = my_dict)

def punejobs(request):
    pune_list = Punejobs.objects.all()
    my_dict = {'pune_list': pune_list}
    
    return render(request, 'jobApp/punejobs.html', context = my_dict)

def chennaijobs(request):
    chennai_list = Chennaijobs.objects.all()
    my_dict = {'chennai_list': chennai_list}
    
    return render(request, 'jobApp/chennaijobs.html', context = my_dict)

def banglorejobs(request):
    bang_list = Banglorejobs.objects.all()
    my_dict = {'bang_list': bang_list}
    
    return render(request, 'jobApp/banglorejobs.html', context = my_dict)