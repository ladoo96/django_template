# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from dbApp.models import Employee

# Create your views here.
def index(request):
    return render(request, 'dbApp/index.html')
def empinfo(request):
    emp_list = Employee.objects.all()
    my_dict = {'emp_list': emp_list}
    
    return render(request, 'dbApp/emp.html', context = my_dict)