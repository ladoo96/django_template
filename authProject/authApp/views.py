# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request,'html/home.html')

def signup(request):
    return render(request,'registration/signup.html')