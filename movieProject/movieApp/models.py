# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Movie(models.Model):
    rdate = models.DateField()
    moviename = models.CharField(max_length = 30, unique = "True")
    hero = models.CharField(max_length = 30)
    heroine = models.CharField(max_length = 30)
    rating = models.IntegerField()
